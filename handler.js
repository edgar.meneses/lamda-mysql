'use strict';
const controller = require('./controllers/controllerTest');

module.exports.findAll = async (event, context) => {

  try{

    const data = await controller.findAll(event, context);
    return getResponse("Ok", 200, data);
  }catch(e) {
    return getResponse("Error Al consultar los datos", 500, null, e);
  }
};

const getResponse = function (message, statusCode, data, error) {
  return {
    statusCode,
    body: JSON.stringify({
      message,
      data,
      error
    })
  }
};