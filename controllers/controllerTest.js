const connection = require('../config/mysqlConection');
const queryString = require('querystring');

module.exports.findAll = function(event, context) {
    return new Promise((resolve, reject) => {
        context.callbackWaitsForEmptyEventLoop = true;
        const sql = 'SELECT * FROM tbl_prueba';
        connection.query(sql, (err, data) => {
            if(err){
                reject(err);
            }else {
                resolve(data);
            }
        });
    });
};