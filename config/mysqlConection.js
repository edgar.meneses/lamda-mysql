const mysql = require('mysql');

const configDb = {
    host:"motodb.c4ouajcms5al.us-east-1.rds.amazonaws.com",
    user:"motodb",
    password:"motodb9113",
    port:3306,
    database:"motodb",
    debug: true
};

function initConecction(config) {
    function addDisconnectHandler(connection) {
        connection.on("error", (error) => {
           if(error instanceof  Error) {
               if(error.code === "PROTOCOL_CONNECTION_LOST"){
                   console.error(error.stack);
                   console.log("Lost connection. Reconnecting...");

                   initConecction(connection.config);
               }else {
                   throw error;
               }
           }
        });
    }
    const connection = mysql.createConnection(config);
    addDisconnectHandler(connection);
    return connection;
}

const connection = initConecction(configDb);

module.exports = connection;